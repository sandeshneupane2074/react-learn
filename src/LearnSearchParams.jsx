import React from 'react'
import { useSearchParams } from 'react-router-dom'

const LearnSearchParams = () => {
  let [params]=   useSearchParams();
  let name = params.get("name")
  let age = params.get("age")
  return (
    <div>
        name is {name}
        <br></br>
age is {age}
    </div>
  )
}

export default LearnSearchParams