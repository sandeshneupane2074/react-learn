import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Home from './Home'
import About from './About'
import Contact from './Contact'
import DynamicRouting from './DynamicRouting'
import LearnSearchParams from './LearnSearchParams'

const Routess = () => {
  return (
    <div>
        <Routes>
            <Route path='/' element={<Home/>}></Route>
            <Route path='/about' element={<About/>}></Route>
            <Route path='/contact' element={<Contact/>}></Route>
            <Route path='/contact/:a ' element={<div>this is dynamic  contact</div>}></Route>
            <Route path='/contact/:a/a1/:1' element={<DynamicRouting/>}></Route>
            <Route path='/product' element={<LearnSearchParams/>}></Route>
        </Routes>

    </div>
  )
}

export default Routess