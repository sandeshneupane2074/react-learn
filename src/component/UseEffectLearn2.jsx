import React, { useEffect, useState } from 'react'

const UseEffectLearn2 = () => {

    let [count ,setCount] = useState(0);

    useEffect(()=>{
        console.log("it is asychnronous so it will show after return ");

        return()=>{
            console.log("i am return");
        }
    },[count])
    console.log("run faster in console before use effect");
  return (
    <div style={{border:"2px solid red"}}>
      <div className="container"></div>

          {count} <br/>
          <button onClick={()=>{
            setCount(count +1)
          }}>
            buttom
          </button>

    </div>
  )
}

export default UseEffectLearn2

/* inside useEffect
=> first return function will execute and above return will execute
=> in 1st rendeer return function will not execute=>from second rendeer return will execute  
when a component of useeffect  is removed return code of useeffect will only execute 
*/
/* 
component did mount .................
component did update ................          life cycle Method
component did unmount .................

return function inside useEffect is clean up function 

use State is used to declared variable and state its value*/
