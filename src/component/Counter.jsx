import React, { useState } from 'react'

const Counter = () => {

   /*  let name ="sandesh"
    let obj ={
        age:23,
        ...name?{name:name}:{}
    }
 */
    let [count, setCount] = useState(0);
  return (
    <div>
        {count}

        <button onClick={()=>{
            setCount(count+1);
        }}
       > add</button>

        <button onClick={()=>{
            setCount(count-1)
        }}>sub</button>
        
        <button onClick={()=>{
            setCount(0)
        }}>del</button>

    </div>
  )
}

export default Counter

//props drilling => normal props pathauda parent to child fere child parent huncha ani soon which causes problem so it is props 
