
import React, { useEffect, useState } from 'react'

const UseEffectLearn = () => {

    let [count1, setCount1] = useState(0);
    let [count2, setCount2] = useState(100);
    let [count3, setCount3] = useState(200);

    useEffect(()=>{

        console.log("sandesh");  
    },[count1,count2]);


  return (
    <div>

        learn use Effect <br/>
        count1 is{count1} <br/>
        count2 is {count2}<br/>
        count3 is {count3} <br/>

        <button onClick={()=>{
            setCount1(count1+1)
        }}> Increase count1
            </button><br/>
        <button  onClick={()=>{
            setCount2(count2+1)
        }}>Increase count2</button><br/> 
        <button onClick={()=>{
            setCount3(count3+1)
        }}>Increase count3</button>


    </div>
  )
}

export default UseEffectLearn;
/* when does useeEffect function run
if there is dependency
dependency change xa bhanea run hunxa nabhayea hudeina
At first render
from second render function will run if any dependency is changed
if there is no dependency then it will run anytime
if there is not dependency does not exist 
for every render function will run
*/
