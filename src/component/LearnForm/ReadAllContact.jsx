import React, { useEffect, useState } from 'react'
import { baseurl } from '../../config/Config'
import axios from 'axios';

const ReadAllContact = () => {

    let[contacts, setContact]=useState([])
    let readallcontact = async ()=>{
        let info = {
            url:`${baseurl}/contacts`,
            method:"get",
        };
        let result =  await axios(info);
        setContact(result.data.data.results);
       
    }
    useEffect(()=>{
        readallcontact();
    },[])
   
    let deleteContact =async(_id)=>{

      let info = {
        url:`${baseurl}/contacts/${_id}`,
        method:"delete",
      };
      let result = await axios(info)
    }
  return (
    <div>
        {
          contacts.map((item, i)=>{
            return (
                <diV key={i} style = {{border:"solid red 2px"}}>
                    <p> FullName :{item.fullName}</p>
                    <p> E-mail :{item.email}</p>
                    <p> address :{item.address}</p>
                    <p> phoneNumber :{item.phoneNumber}</p>
                    <br/>
<button onClick={async()=>{
     await deleteContact(item._id);
     await readallcontact();
}}>delete

</button>

                </diV>
            )
          })
        }

    </div>
  )
}

export default ReadAllContact

//