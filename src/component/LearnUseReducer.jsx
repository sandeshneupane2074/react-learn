import React, { useReducer } from 'react'

const LearnUseReducer = () => {
    let initialValue = 0;
    let reducer = (state,action) =>{
        console.log("state is", state);
        console.log("action is ",action);
        return 9;
    }

    let[state, dispatch] = useReducer(reducer,initialValue)
  return (

    <div>useReducer<br></br>
{state}
<br/>
<button onClick={()=>{
    dispatch("ram");
}}> click me use reducer

</button>
    </div>
  )
}

export default LearnUseReducer