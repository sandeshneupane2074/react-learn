import React, { useContext } from 'react'
import { AgeContext } from '../Nitan'

const LearnUseContext2 = () => {
    let age = useContext(AgeContext)
  return (
    <div>LearnUseContext2
        <br/>
        Age of Nitan is  : {age}
    </div>
  )
}

export default LearnUseContext2