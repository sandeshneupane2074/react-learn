import React, { useContext } from 'react'
import { InfoContext } from '../Nitan'

const LearnUseContext1 = () => {
    let data = useContext(InfoContext);
  return (
    <div>LearnUseContext1 <br></br>
    <p>Usecontext data imported  from Nitan.jsx on learn usecontext1 </p> <br/>  
        {data}
    </div>
  )
}

export default LearnUseContext1