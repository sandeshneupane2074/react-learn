import React, { useRef } from 'react'

const LearnUseRef = () => {
    let inputRef1 = useRef();
    let inputRef2 = useRef();
    let inputRef3 = useRef();
    
      return (
    <div>
        <p ref={inputRef1}>One Piece</p>
        <p ref={inputRef2}>Demon Slayer :sword smith village</p>
<button
onClick={()=>{
    
    inputRef1.current.style.background = "red"
    inputRef2.current.style.background = "blue"
}}> UseRef

</button>
<br/>
<input ref={inputRef3}></input>
<button onClick={()=>{
    inputRef3.current.focus();
}}>input focus 

</button>
<br></br>
    </div>
  )
}

export default LearnUseRef