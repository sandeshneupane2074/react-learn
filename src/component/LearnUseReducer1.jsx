import React, { useReducer } from 'react'

const LearnUseReducer1 = () => {
    let initialValue = 0;
    let reducer = (state, action)=> {
        if(action.type === "increment"){
return state +1;
        } else if(action.type ==="decrement"){
            return state - 1;
        } else if(action.type ==="reset"){
return initialValue;
        }
        return state; 
    };
    let [count ,dispatch]=useReducer(reducer, initialValue);
    

  return (
 
    <div>LearnUseReducer1<br/>
    {count}
    <button onClick={()=>{
         dispatch({type :"increment"})

    }}>Increment
        </button><br/>
    <button onClick={()=>
    {
        dispatch({type:"decrement"})
    }}
    >Decrement</button> <br/>
    <button onClick={()=>{
dispatch({type:"reset"})
    }}
    >Reset</button> <br/>

    </div>
  )
}

export default LearnUseReducer1

//in use reducer inside reducer function it must return next  value