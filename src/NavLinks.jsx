import React from 'react'
import { NavLink } from 'react-router-dom'

const NavLinks = () => {
  return (
    <div>
          <NavLink to="/" style={{marginLeft: "30px"}}>Home</NavLink>
    <NavLink to="/contact"style={{marginLeft: "30px"}}>Contact</NavLink>
    <NavLink to="/about"style={{marginLeft: "30px"}}>About</NavLink>
    <NavLink to="/product"style={{marginLeft: "30px"}}>Products</NavLink>
    </div>
  )
}

export default NavLinks